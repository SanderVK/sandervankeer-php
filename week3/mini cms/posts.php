<?php


    $conn = new PDO('mysql:host=localhost; dbname=phples','root','');

    if( !empty($_POST) )
    {
        $article = $_POST['article'];
        $title=$_POST['title'];
        $sql = "INSERT INTO blogpost (article,title) VALUES ('$article','$title')";
        $statement = $conn->prepare("INSERT INTO blogpost (article,title) VALUES(:article,:title)");
        $statement->bindValue(":article",$article);
        $statement->bindValue(":title", $title);
        $statement->execute();
        
        
    }

    $select = "SELECT * FROM blogpost ORDER BY id DESC ;";
    $posts= $conn->query($select);

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Your posts</title>
    
    <style>
    
        #container
        {
            margin-right: auto;
            margin-left: auto;
            width:400px;
        }
        
        
    </style>
    
</head>
<body>
  
  <div id="container">
   
    <?php
        while($post= $posts->fetch(PDO::FETCH_ASSOC))
        {
            echo "<h1>".htmlspecialchars($post['title'])."</h1>";
            
            echo "<p>".htmlspecialchars($post['article'])."</p><hr>";
        }
    
    ?>
    
    
    
    </div>
    
    
</body>
</html>