<?php

if(!isset($_SESSION['cart'] ) ) 
   { 
        $_SESSION['cart']=array();
   }

?>


<p>Items in your cart:</p>
<p>You have <?php echo count($_SESSION['cart'])?> items in your cart</p>


<ul>
    
    <?php foreach ($_SESSION['cart'] as $p_id){ ?>
        <li>
            <?php echo $products[$p_id]['name']."  €" . number_format($products[$p_id]['price'], 2, ',', ''); ?>
        </li>
    <?php } ?>   
    
</ul> 