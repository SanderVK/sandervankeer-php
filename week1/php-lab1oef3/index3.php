<?php

function cmp(array $a, array $b) 
{
    if ($a['duration'] < $b['duration']) 
    {
        return -1;
    } 
else if ($a['duration'] > $b['duration']) 
    {
        return 1;
    } 
else 
   {
        return 0;
    }
};

$listItem=[
 
    ["duration"=>"25",
    "name" => "French",
    "subject" => "Homework"
    
    ],
    
    [
        
    "duration"=>"1,5",
    "name" => "Guitar lesson",
    "subject" => "Hobby"
    
    
    ],
    
        [
        
    "duration"=>"13",
    "name" => "Art",
    "subject" => "Hobby"
    
    
    ],
    
    ["duration"=>"1",
    "name" => "math",
    "subject" => "Homework"
    
    ],
    
    
    
        [
        
    "duration"=>"8",
    "name" => "Sam swimming lesson",
    "subject" => "Kids"
    
    
    ],
    
    
]


?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test jezelf 3 - To-do</title>
    
    
    <style>
    
        
    .p1
        {
            font-size:200%;
        }
        
    .p2  
        {
            font-size:150%;
        }
        
    .red
        {
            background-color:red;
            margin-right:auto;
            margin-left:auto;
            width: 200px;
        }
        
    .orange
        {
            background-color:orange;
            margin-right:auto;
            margin-left:auto;
            width: 200px;
        }
    
    .green
        {
            background-color:green;
            margin-right:auto;
            margin-left:auto;
            width: 200px;
        }    
    
    </style>
    
    
</head>
<body>
   
   <?php
    
    
    
    $kleur="";
    usort($listItem, 'cmp');

    foreach ($listItem as $li){
        
    if ( $li["duration"] <=2)
        
    {
        $kleur= "red";  
        
        
    }
        
    elseif(  $li["duration"] <= 24)
       
    {
        $kleur= "orange";   
    }    
        
    else 
    {
        $kleur="green";
    }
       
    ?>
    
  
  
    <div class=<?php echo $kleur;?>> 
    
        <p class="p1"><?php echo $li["name"];?></p>
        <p class="p2"><?php echo $li["subject"]; ?></p>
        <p class="p3"><?php echo "deadline: " .$li["duration"]. "h";?></p>
    
    </div>
    

    <?php }?>
    
    
   
    
</body>
</html>