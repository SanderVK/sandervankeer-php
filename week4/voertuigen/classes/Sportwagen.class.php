<?php

    include_once("voertuig.class.php");
    include_once("Db.class.php");

    class Sportwagen extends  Voertuig
    {
        private $m_bStereoInstallatie;

        public function __set($p_sProperty, $p_vValue)
        {

            parent::__set($p_sProperty,$p_vValue);


            switch($p_sProperty)
            {

                case "Stereo":
                    $this->m_bStereoInstallatie = $p_vValue;
                    break;
            }
        }

        public function __get($p_sProperty)
        {
          $vResult =  parent::__get($p_sProperty);

            switch($p_sProperty)
            {
                case "Stereo":
                   $vResult = $this->m_bStereoInstallatie;
                  break;
            }

            return $vResult;
        }

        public function Reserveer()
        {
            if (date('G') > 12) {

                throw new Exception ("Reservatie moet voor 12 uur gebeuren!");
            } else {
                $conn = Db::getInstance();
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                // define query, prepared statements
                $statement = $conn->prepare("INSERT INTO voertuigen(merk,aantalPassagiers,aantalDeuren,stereo) VALUES (:Merk,:AantalPassagiers,:AantalDeuren,:Stereo)");
                $statement->bindValue(":Merk", $this->Merk);
                $statement->bindValue(":AantalPassagiers", $this->AantalPassagiers);
                $statement->bindValue(":AantalDeuren", $this->AantalDeuren);
                $statement->bindValue(":Stereo", $this->Stereo);
                echo "Reservatie gelukt!";

                $statement->execute();

            }
        }

    }