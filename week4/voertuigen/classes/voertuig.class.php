<?php
    include_once("iMachine.class.php");
    include_once("Db.class.php");
    abstract class Voertuig implements iMachine
    {
        private $m_sMerk;
        private $m_iAantalPassagiers;
        private $m_iAantalDeuren;




        public function __set($p_sProperty, $p_vValue)
        {
            switch ($p_sProperty)
            {
                case "Merk":
                    $this->m_sMerk = $p_vValue;
                    break;

                case "AantalPassagiers":
                    $this->m_iAantalPassagiers = $p_vValue;
                    break;
                case"AantalDeuren":
                    $this->m_iAantalDeuren = $p_vValue;

            }
        }


        public function __get($p_sProperty)
        {
            $vResult= null;
            switch ($p_sProperty)
            {
                case "Merk":
                    $vResult =  $this->m_sMerk;
                    break;

                case "AantalPassagiers":
                    $vResult =  $this->m_iAantalPassagiers;
                    break;

                case "AantalDeuren":
                    $vResult =  $this->m_iAantalDeuren;
                    break;
            }
            return $vResult;
        }


    }

