<?php

    include_once ("voertuig.class.php");
    include_once("Db.class.php");

    class Vrachtwagen extends Voertuig
    {
        private $m_iMaxLast;

        public function __set($p_sProperty, $p_vValue)
        {
                parent:: __set($p_sProperty, $p_vValue);

            switch ($p_sProperty)
            {
                case "MaxLast":
                    $this->m_iMaxLast = $p_vValue;
                    break;
            }
        }

        public function __get($p_sProperty)
        {

            $vResult = parent::__get($p_sProperty);

            switch ($p_sProperty)
            {
                case "MaxLast":
                    $vResult = $this->m_iMaxLast;
                    break;
            }

            return $vResult;
        }

        public function Reserveer()
        {
            if (date('G') > 12) {

                throw new Exception ("Reservatie moet voor 12 uur gebeuren!!");
            } else {
                $conn = Db::getInstance();
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                // define query, prepared statements
                $statement = $conn->prepare("INSERT INTO voertuigen(merk,aantalPassagiers,aantalDeuren,maxLast) VALUES (:Merk,:AantalPassagiers,:AantalDeuren,:MaxLast)");
                $statement->bindValue(":Merk", $this->Merk);
                $statement->bindValue(":AantalPassagiers", $this->AantalPassagiers);
                $statement->bindValue(":AantalDeuren", $this->AantalDeuren);
                $statement->bindValue(":MaxLast", $this->MaxLast);

                // shit works till here

                $statement->execute();
            }
        }

    }