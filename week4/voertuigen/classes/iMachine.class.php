<?php

    interface iMachine
    {
        public function  __set( $p_sProperty, $p_vValue );
        public function __get($p_sProperty);
        public function Reserveer();
    }