<?php

    include_once("classes/Vrachtwagen.class.php");
    include_once("classes/Sportwagen.class.php");

    $v = new Vrachtwagen();
    $s = new Sportwagen();

    if(!empty($_POST))
    {
        try{

               if ($_POST["soort"] = "Vrachtwagen") {
                   $v->Merk = $_POST["merk"];
                   $v->AantalPassagiers = $_POST["passagiers"];
                   $v->AantalDeuren = $_POST["deuren"];
                   $v->MaxLast = $_POST["MaxLast"];
                   $v->Reserveer();


               }
            else{
                $s->Merk = $_POST["merk"];
                $s->AantalPassagiers = $_POST["passagiers"];
                $s->AantalDeuren = $_POST["deuren"];
                $s->Stereo = $_POST["stereo"];

                $s->Reserveer();


            }


                    
            }

            catch (Exception $e)
            {
                $error = $e->getMessage();
            }
    }


?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Voertuigen!</title>
    
    <style>
    
    #passagiers, #deuren
    {
        margin-top: 5px;
            
    }
        
    #btn
    {
        margin-top: 5px;
    }

    .error
    {
        font-size: 200%;
        display: block;
        color:#ffffff;
        background-color: red;
        width: 300px;
        height: 100px;;

    }
    
    </style>

    <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#stereo").hide();
            $("#lblStr").hide();
            $("select").change(function(){
                $( "select option:selected").each(function(){
                    if($(this).attr("value")=="Vrachtwagen"){
                        $("#stereo").hide();
                        $("#lblStr").hide();
                        $("#maxLast").show();
                        $("#lblLast").show();
                    }
                    if($(this).attr("value")=="Sportwagen"){
                        $("#maxLast").hide();
                        $("#lblLast").hide();
                        $("#stereo").show();
                        $("#lblStr").show();

                    }
                });
            });
        });
    </script>

</head>
<body>
<?php if(isset($error)): ?>
        <div class="error">
            <?php echo $error ?>
        </div>
<?php  endif; ?>


   <h1>Kies hier je voertuig</h1>
   
   <form action="" method="post">

       <select name="soort" id="soort" >
           <option value="Vrachtwagen">Vrachtwagen</option>
           <option value="Sportwagen">Sportwagen</option>
       </select>
       <br>
       <label for="merk">Merk:</label>
       <input type="text" id="merk" name="merk">
       <br>
       <label for="passagiers">Aantal passagiers:</label>
       <input type="text" id="passagiers" name="passagiers">
       <br>
       <label for="deuren">Aantal deuren:</label>
       <input type="text" id="deuren" name="deuren">
       <br>
       <label for="stereo" id="lblStr">Stereo:</label>
       <input value="1" type="checkbox" id="stereo" name="stereo"/>
       <br>
       <label for="maxLast" id="lblLast">Maximum laadvermogen (ton):</label>
       <input type="text" id="maxLast" name="MaxLast">
       <br>
       <button type="submit" id="btn">Reserveer!</button>

   </form>


    
</body>
</html>